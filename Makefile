$PHONEY: all
all: dotnetcat.exe

dotnetcat.exe: dotnetcat.cs
	mcs dotnetcat.cs

@PHONEY: clean
clean: 
	-rm -f dotnetcat.exe
